/**
* Anuncio Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	AnuncioModel = Backbone.Model.extend({
	        defaults: {
	        		id: 0
	        	,	sku: 0
	        	,	titulo: ''
	        	,	descripcion_corta: ''
	        	,	descripcion_detallada: ''
	        	,	razon_social: ''
	        	,	subcategorias: []
	        	,	precio: ''
	        	,	descuento: ''
	        	,	imagenes: []
	        	,	direccion: ''
	        	,	id_user: 0
	        	,	enlace: ''
	        	,	plan: ''
	        	,	coordenadas: ''
	        	,	virtuemart_product_price_id: 0
	        }

	    ,   initialize: function(){
	            
	        }


	     	/**
	        * Instance controller to send mail
	        *
	        */
	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.save'
					,	data: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	selectSubcategorias: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.selectSubcategorias'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	getPlanes: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.getPlanes'

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	unpublish: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.unpublish'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	publish: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.publish'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	unpublishAnuncios: function( ids, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.unpublishAnuncios'
					,	ids: ids

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	publishAnuncios: function( ids, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.publishAnuncios'
					,	ids: ids

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	deleteAnuncio: function( id, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.deleteAnuncio'
					,	id: id

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	reactivarPlan: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.reactivarPlan'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );